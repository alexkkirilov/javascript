"use strict";
jQuery(document).ready(function ($) {
//Math Symbol unicode
    var arrow = '\u2190';
    var sqrt = '\u221A';
    var plusMinus = '\u00B1';

    var formulaScreen = "#viewFormula";
    var inputScreen = '#viewCurrentNumber';

    var input = "";
    var output = "";
    var checkForDivideChar = "";

    var result;
    var countEnter = 0;
    var lbracket = 0;
    var rbracket = 0;

    var MathSymbolPressed = false;
    var numberPressed = false;
    var enterPressed = false;
    var openBraked = false;
    var lBrackets = false;
    var rBrackets = false;
    var enterForBrackets = false;
    var sqrtPressed = false;

    //Buttons from the screen
    $("td").on('click', function (event) {
        KeyPressFunc(event.target.innerHTML);
    });

    //Keyboard button presses
    $(document).keyup(function (event) {
        var x = event.keyCode;
        if (x === 13) {
            KeyPressFunc("=");
        } else if (x < 48 && x < 57) {
            KeyPressFunc(String.fromCharCode(x)); //Normal digit from the keyboard 
        } else if (x > 95 && x < 106) {
            KeyPressFunc(String.fromCharCode(x - 48)); //NumPad form 0 to 9
        } else if (x >= 106 && x < 112) {
            KeyPressFunc(String.fromCharCode(x - 64)); //NumPad Math symbols
        } else {
            event.preventDefault(); //If it`s not a number or Math symbol
        }
    });

    function KeyPressFunc(key) {
        if (output.length < 85) {
            console.log(key);
            switch (key) {
                case "=":
                    (countEnter === 2) ? clearFields(countEnter = 0) : countEnter++; //Clear all field on third click
                    if (!MathSymbolPressed) {
                        if (!openBraked) {
                            output += input;
                            printResult(output);
                            $(formulaScreen).text(output);
                            MathSymbolPressed = true;
                            enterForBrackets = true;
                            enterPressed = true;
                            openBraked = false;
                            lbracket = 0;
                            rbracket = 0;
                        }
                    }
                    break;

                case arrow:
                    //Delete last Digit
                    input = (input.substring(0, input.length - 1));
                    if (input === "") {
                        input = "0"
                    }
                    $(inputScreen).val(input);
                    break;

                case "C":
                    clearFields();
                    MathSymbolPressed = true;
                    numberPressed = false;
                    openBraked = false;
                    enterPressed = false;
                    lBrackets = false;
                    rBrackets = false;
                    sqrtPressed = false;
                    break;

                case "CE":
                    //Delete the last input
                    input = "0";
                    $(inputScreen).val(input);
                    sqrtPressed = true;
                    numberPressed = false;
                    MathSymbolPressed = true;
                    break;

                case plusMinus:
                    input *= -1;
                    $(inputScreen).val(input);
                    break;

                case sqrt:
                    if (output.slice(-1) === ")") {
                        var lastOpenBracket = output.split("(", lbracket).join("(").length;
                        var lastPos = output.indexOf(')');
                        var temp1 = output.substring(lastOpenBracket, lastPos + 1); //Remove sqrt formulam from output screen
                        input = Math.sqrt(eval(temp1));
                        output = output.replace(temp1, input);
                        if (eval(output)) {
                            input = eval(output);
                        }
                        lbracket--;
                        rbracket--;
                        $(formulaScreen).text(output);
                        $(inputScreen).val(input);
                        sqrtPressed = true;
                    } else {
                        if (input != 0) {
                            input = "" + Math.sqrt(parseFloat(input));
                            $(formulaScreen).text(output + " " + input);
                            sqrtPressed = true;
                        }
                    }
                    break;
                case "(":
                    if (enterForBrackets) {
                        output = "";
                        enterForBrackets = false;
                    }
                    if ((output === "" || MathSymbolPressed || lBrackets) && !enterForBrackets) {
                        output += "( ";
                        lbracket++;
                        lBrackets = true;
                        rBrackets = false;
                        openBraked = true;
                        MathSymbolPressed = false;
                        $(formulaScreen).text(output);
                        $(inputScreen).val(input);
                    }
                    break;

                case ")":
                    if ((lbracket > rbracket) && (numberPressed || rBrackets)) {
                        (rBrackets) ? output += " )" : output += input + " )";
                        rbracket++;
                        rBrackets = true;
                        lBrackets = false;
                        numberPressed = false;
                        MathSymbolPressed = false;
                        $(formulaScreen).text(output);
                        if (lbracket === rbracket) {
                            openBraked = false;
                            printResult(output.toString()); //Print result
                            input = "";
                        }
                    }
                    break;
                case ".":
                    if (input === "") {
                        input = "0";
                    }
                    if (input.indexOf('.') === -1) {
                        input += key;
                        $(inputScreen).val(input);
                        numberPressed = true;
                        MathSymbolPressed = true;
                        enterForBrackets = false;
                    }
                    break;
                default:
                    console.log((numberPressed && input != 0) || lBrackets || rBrackets || enterPressed);
                    if (isFinite(key)) {
                        if (input === "" || lBrackets || MathSymbolPressed || enterPressed || numberPressed) {
                            if (input === "0" || enterPressed || sqrtPressed) {
                                input = "";
                            }
                            input = input.toString();
                            if (input.length < 13) {
                                checkForDivideChar = output.slice(-2);
                                if (checkForDivideChar === '/ ' && key === "0") {
                                    clearFields("Cannot divide by zero");
                                } else {
                                    $(inputScreen).val(input += key);
                                    numberPressed = true;
                                    MathSymbolPressed = false;
                                    enterForBrackets = false;
                                    sqrtPressed = false;
                                    countEnter = 0;
                                }
                            }
                        }
                    } else if ((numberPressed && input != 0) || lBrackets || rBrackets || enterPressed) {
                        if (enterPressed) {
                            (lBrackets) ? output : output = "";
                            enterPressed = false;
                            $(formulaScreen).text(output);
                        }
                        if ((input.length > 13 || input === "")) {
                            input = "0";
                        }
                        checkForDivideChar = output.slice(-2);
                        if (checkForDivideChar === '/ ' && input === "0") {
                            clearFields("Cannot divide by zero");
                        } else if (numberPressed && !enterPressed || lBrackets || rBrackets) {
                            if (lBrackets) {
                                output += input;
                                input = "";
                                rBrackets = false;
                                $(formulaScreen).text(output += " " + key + " ");
                            } else {
                                (rbracket) ? output : output += input;
                                printResult(output); //Print result
                                input = "0";
                                $(formulaScreen).text(output += " " + key + " ");
                            }
                        }
                        if (numberPressed) {
                            numberPressed = false;
                            enterPressed = false;
                            MathSymbolPressed = true;
                            sqrtPressed = false;
                        }
                    }
            }
        }
    }

    function clearFields(inputPrintValue) {
        if (inputPrintValue == null) {
            inputPrintValue = "0";
        }
        input = "";
        output = "";
        result = "";
        $(formulaScreen).text(output);
        $(inputScreen).val(inputPrintValue);
    }
    function printResult(calculate) {
        result = parseFloat(eval(calculate.toString()));
        result.toPrecision(13).replace(/\.([^0]+)0+$/, ".$1");
        $(inputScreen).val(result.toString());
    }
    function getPosition(str, m, i) {
        return str.split(m, i).join(m).length;
    }
});