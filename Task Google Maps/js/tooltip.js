jQuery(document).ready(function ($) {
    var div = document.getElementById('phoneNumberToolTip');
    var a = document.getElementById("phoneNumber");
    
    //clear the msg if have some
    $("input").focus(function(){
        document.getElementById('msg').innerHTML = '';
    });
    
    //Phone number hint - how to fill correctly
    var fadeSpeed = 25; // a value between 1 and 1000 where 1000 will take 10
    var newOpacity;
// seconds to fade in and out and 1 will take 0.01 sec.
    var tipMessage = "0888-888-888 <br /> 088-888-8888 <br /> 0888888888 <br /> +359888888888 <br /> (+359)888-888-888 <br /> (+359)888884444";
    var showTip = function () {
        var tip = document.createElement("span");
        tip.className = "tooltip";
        tip.id = "tip";
        tip.innerHTML = tipMessage;
        div.appendChild(tip);
        tip.style.opacity = "0"; // to start with...
        var intId = setInterval(function () {
            newOpacity = parseFloat(tip.style.opacity) + 0.1;
            tip.style.opacity = newOpacity.toString();
            if (tip.style.opacity == "1") {
                clearInterval(intId);
            }
        }, fadeSpeed);
    };
    var hideTip = function () {
        var tip = document.getElementById("tip");
        var intId = setInterval(function () {
            newOpacity = parseFloat(tip.style.opacity) - 0.1;
            tip.style.opacity = newOpacity.toString();
            if (tip.style.opacity == "0") {
                clearInterval(intId);
                tip.remove();
            }
        }, fadeSpeed);
        tip.remove();
        $("#phoneNumberToolTip").hide();
    };
    a.addEventListener("mouseover", showTip, false);
    a.addEventListener("mouseout", hideTip, false);
});