function validationForm() {
    var name = document.getElementById('fullName').value;
    var address = document.getElementById('address').value;
    var mail = document.getElementById('email').value;
    var phone = document.getElementById('phoneNumber').value;
    var url = document.getElementById('homepage').value;
    //Check all field - not empty
    if (name !== "" && address !== "" && mail !== "" && phone !== "" && url !== "") {
        if (fullNameCheck(name) && addressCheck(address) && mailCheck(mail) && telephoneCheck(phone) && urlCheck(url)) {
            document.getElementById('fullName').style = "border-color:none";
            document.getElementById('address').style = "border-color:none";
            document.getElementById('email').style = "border-color:none";
            document.getElementById('phoneNumber').style = "border-color:none";
            document.getElementById('homepage').style = "border-color:none";
            return true;
        } else {
            document.getElementById('msg').innerHTML = 'Please, fill all fields correctly';
            if (!fullNameCheck(name))
                document.getElementById('fullName').style = "border-color:red";
            if (!addressCheck(address))
                document.getElementById('address').style = "border-color:red";
            if (!mailCheck(mail))
                document.getElementById('email').style = "border-color:red";
            if (!telephoneCheck(phone))
                document.getElementById('phoneNumber').style = "border-color:red";
            if (!urlCheck(url))
                document.getElementById('homepage').style = "border-color:red";
        }
    } else {
        document.getElementById('msg').innerHTML = 'Fill all fields!';
    }
}

//Name validation
function fullNameCheck(name) {
    var isName = /^[a-zA-Z ]{2,30}$/.test(name);
    return isName;
}

//Phone validaion
function telephoneCheck(phone) {
    //true -> 0877-664-416 / 087-766-4416 / 0877664416 / +359877664416 / (+359)877-664-416 / (+359)877664416
    var regExp = /^\(+[0-9]{3}\)[0-9]{9}|[0-9]{10}|[0-9]{4}-[0-9]{3}-[0-9]{3}|[0-9]{3}-[0-9]{3}-[0-9]{4}|\(\+[0-9]{3}\)[0-9]{9}|\(\+[0-9]{3}\)[0-9]{3}-[0-9]{3}-[0-9]{3}$/;
    var phones = regExp.test(phone);
    return phones;
}

//Address validation
function addressCheck(address) {
    var isAddress = /^[a-zA-Z0-9\s\,,-;."'-]*$/.test(address);
    return isAddress;
}

//Email validation
function mailCheck(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//URL validation
function urlCheck(url) {
    var isURL = /^(ftp|http|https):\/\/[^ "]+$/.test(url);
    return isURL;
}
