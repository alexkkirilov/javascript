jQuery(document).ready(function ($) {
    $(document).on("click", "#findLocation_btn", function () {

        var geocoder = new google.maps.Geocoder();
        
        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerPosition(latLng) {
        }

        function updateMarkerAddress(str) {
            document.getElementById("address").value = str;
        }

        function initialize() {
            var marker;
            var latLng = new google.maps.LatLng(42.708742, 23.300850);
            var map = new google.maps.Map(document.getElementById('googleMap'), {
                zoom: 13,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            // Update current position info.
            updateMarkerPosition(latLng);
            geocodePosition(latLng);
            /****/
            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
                geocodePosition(marker.getPosition());
            });

            function placeMarker(location) {
                if (marker) {
                    marker.setPosition(location);
                } else {
                    marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                }
            }
        }
        initialize();
        $("#map-container-right").css({"display": "block"});
        var geocoder = new google.maps.Geocoder();
    });
});