"use strict";
jQuery(document).ready(function ($) {
    $("#map-container-right").css({"display": "none"});
    $("#phoneNumberToolTip").hide();
    var map;
    var geocoder;
    //Google Maps API
    function initialize() {
        map = new google.maps.Map(document.getElementById('googleMap'), {
            zoom: 15,
            center: {lat: 42.708742, lng: 23.300850}
        });
        geocoder = new google.maps.Geocoder();
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
    
    $(document).on("click", "#submit_btn", function () {
        if (validationForm()) {
            //Set data in LocalStorage
            setUserData();
            //Show the map
            $("#map-container-right").css({"display": "block"});
            initialize();
            //Set Position on the map
            geocodeAddress(geocoder, map);
        }
    });
    
    //Phone number hint
    //true -> 0877-664-416 / 087-766-4416 / 0877664416 / +359877664416 / (+359)877-664-416 / (+359)877664416
    $(document).on("mouseover","#phoneNumber", function(){
        $("#phoneNumberToolTip").show();
    });
});


//Set data in local storage
function setUserData() {
    var person = {
        name: document.getElementById('fullName').value,
        address: document.getElementById('address').value,
        email: document.getElementById('email').value,
        phone: document.getElementById('phoneNumber').value,
        website: document.getElementById('homepage').value
    };

    localStorage.person = JSON.stringify(person);
    //Print the data from local storage
//    var personObj = JSON.parse(localStorage.person);
//    console.log("Name: " + personObj.name + " Address: " + personObj.address + " "
//            + "E-mail: " + personObj.mail + " Phone: " + personObj.phone + " "
//            + "WebSite: " + personObj.website);
}