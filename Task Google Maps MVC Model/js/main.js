/* global google */

"use strict";
jQuery(document).ready(function ($) {
    $("#map-container-right").css({"display": "none"});
    $("#phoneNumberToolTip").hide();
    var Model = {
        data: {
            map: null,
            geocoder: null,
            marker: null,
            latLng: null,
            geoAddress: null,
            booleans: {
                isSubmit: false,
                isValid: false,
                isName: false,
                isPhones: false,
                isAddress: false,
                isEmail: false,
                isURL: false
            },
            person: {
                name: "",
                address: "",
                email: "",
                phone: "",
                website: ""
            },
            regExp: {
                name: /^[a-zA-Z ]{2,30}$/,
                address: /^[a-zA-Z0-9\s\,,-;."'-]*$/,
                //true -> 0877-664-416 / 087-766-4416 / 0877664416 / +359877664416 / (+359)877-664-416 / (+359)877664416
                phoneNum: /^\(+[0-9]{3}\)[0-9]{9}|[0-9]{10}|[0-9]{4}-[0-9]{3}-[0-9]{3}|[0-9]{3}-[0-9]{3}-[0-9]{4}|\(\+[0-9]{3}\)[0-9]{9}|\(\+[0-9]{3}\)[0-9]{3}-[0-9]{3}-[0-9]{3}$/,
                mail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                url: /^(ftp|http|https):\/\/[^ "]+$/
            }
        },
        setDataPerson: function () {
            this.data.person.name = document.getElementById('fullName').value;
            this.data.person.address = document.getElementById('address').value;
            this.data.person.email = document.getElementById('email').value;
            this.data.person.phone = document.getElementById('phoneNumber').value;
            this.data.person.website = document.getElementById('homepage').value;
        },
        getDataPerson: function () {
            return this.data.person;
        },
        setToLocalStorage: function () {
            localStorage.person = JSON.stringify(this.getDataPerson());
        }
    };

    var View = {
        validation: {
            init: function () {
                $(document).on('focusout', "input", function () {
                    var selected = this.id;
                    var values = $('#' + this.id).val();
                    if (values != "" || values != null) {
                        switch (selected) {
                            case "fullName":
                                Model.data.booleans.isName = Model.data.regExp.name.test(values);
                                View.validation.render(selected, Model.data.booleans.isName);
                                break;
                            case "phoneNumber":
                                Model.data.booleans.isPhones = Model.data.regExp.phoneNum.test(values);
                                View.validation.render(selected, Model.data.booleans.isPhones);
                                break;
                            case "address":
                                Model.data.booleans.isAddress = Model.data.regExp.address.test(values);
                                View.validation.render(selected, Model.data.booleans.isAddress);
                                break;
                            case "email":
                                Model.data.booleans.isEmail = Model.data.regExp.mail.test(values);
                                View.validation.render(selected, Model.data.booleans.isEmail);
                                break;
                            case "homepage":
                                Model.data.booleans.isURL = Model.data.regExp.url.test(values);
                                View.validation.render(selected, Model.data.booleans.isURL);
                                break;
                        }
                        View.validation.submit();
                    }
                });
            },
            render: function (selected, checked) {
                (checked) ? $("#" + selected).css({"border-color": "green"}) : $("#" + selected).css({"border-color": "red"});
            },
            submit: function () {
                (Model.data.booleans.isName && Model.data.booleans.isPhones && Model.data.booleans.isAddress && Model.data.booleans.isEmail && Model.data.booleans.isURL) ? Model.data.booleans.isValid = true : Model.data.booleans.isValid = false;
            }
        },
        //Google Maps API
        mapOnSubmit: {
            initialize: function () {
                Model.data.map = new google.maps.Map(document.getElementById('googleMap'), {
                    zoom: 15,
                    center: {lat: 42.708742, lng: 23.300850}
                });
                Model.data.geocoder = new google.maps.Geocoder();
            },
            geocodeAddress: function (geocoder, resultsMap) {
                Model.data.geoAddress = document.getElementById('address').value;
                geocoder.geocode({'address': Model.data.geoAddress}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        Model.data.marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        },
        mapOnFindLocation: {
            initialize: function () {
                Model.data.latLng = new google.maps.LatLng(42.708742, 23.300850);
                Model.data.map = new google.maps.Map(document.getElementById('googleMap'), {
                    zoom: 13,
                    center: Model.data.latLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                google.maps.event.addListener(Model.data.map, 'click', function (event) {
                    if (Model.data.marker && !Model.data.booleans.isSubmit) {
                        Model.data.marker.setPosition(event.latLng);
                    } else {
                        Model.data.booleans.isSubmit = false;
                        Model.data.marker = new google.maps.Marker({
                            position: event.latLng,
                            map: Model.data.map
                        });
                    }
                    View.mapOnFindLocation.geocodePosition(Model.data.marker.getPosition());
                });
            },
            geocodePosition: function (pos) {
                Model.data.geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        document.getElementById("address").value = responses[0].formatted_address;
                    } else {
                        document.getElementById("address").value = updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }
        }
    };

    var Controller = {
        handler: function () {
            View.validation.init();
            $(document).on("click", "#submit_btn", function () {
                if (Model.data.booleans.isValid) {
                    Model.data.booleans.isSubmit = true;
                    Model.setDataPerson(); //Set Person data
                    Model.setToLocalStorage(); //Set data in LocalStorage
                    $("#map-container-right").css({"display": "block"}); //Show the map
                    View.mapOnSubmit.initialize();
                    View.mapOnSubmit.geocodeAddress(Model.data.geocoder, Model.data.map); //Set Position on the map
                } else {
                    $('input').each(function () {
                        if ($(this).val() == "") {
                            $(this).css({"border-color": "red"});
                        }
                    });
                }
            });
            $(document).on("click", "#findLocation_btn", function () {
                View.mapOnFindLocation.initialize();
                $("#map-container-right").css({"display": "block"});
                Model.data.geocoder = new google.maps.Geocoder();
                Model.data.booleans.isAddress = true;
                View.validation.submit();
            });
            //Phone number hint
            //true -> 0877-664-416 / 087-766-4416 / 0877664416 / +359877664416 / (+359)877-664-416 / (+359)877664416
            $(document).on("mouseover", "#phoneNumber", function () {
                $("#phoneNumberToolTip").show();
            });
        }
    };
    
    Controller.handler();
});
