jQuery(document).ready(function ($) {
//Phone number hint - how to fill correctly
    var toolTipModel = {
        data: {
            div: document.getElementById('phoneNumberToolTip'),
            a: document.getElementById("phoneNumber"),
            // a value between 1 and 1000 where 1000 will take 10 
            // seconds to fade in and out and 1 will take 0.01 sec.
            fadeSpeed: 25,
            newOpacity: 0.0,
            tipMessage: "0888-888-888 <br /> 088-888-8888 <br /> 0888888888 <br /> +359888888888 <br /> (+359)888-888-888 <br /> (+359)888884444",
            tip: null
        }
    };
    var toolTipView = {
        showTip: function () {
            toolTipModel.data.tip = document.createElement("span");
            toolTipModel.data.tip.className = "tooltip";
            toolTipModel.data.tip.id = "tip";
            toolTipModel.data.tip.innerHTML = toolTipModel.data.tipMessage;
            toolTipModel.data.div.appendChild(toolTipModel.data.tip);
            toolTipModel.data.tip.style.opacity = "0"; // to start with...
            var intId = setInterval(function () {
                toolTipModel.data.newOpacity = parseFloat(toolTipModel.data.tip.style.opacity) + 0.1;
                toolTipModel.data.tip.style.opacity = toolTipModel.data.newOpacity.toString();
                if (toolTipModel.data.tip.style.opacity === "1") {
                    clearInterval(intId);
                }
            }, toolTipModel.data.fadeSpeed);
        },
        hideTip: function () {
            toolTipModel.data.tip = document.getElementById("tip");
            var intId = setInterval(function () {
                toolTipModel.data.newOpacity = parseFloat(toolTipModel.data.tip.style.opacity) - 0.1;
                toolTipModel.data.tip.style.opacity = toolTipModel.data.newOpacity.toString();
                if (toolTipModel.data.tip.style.opacity === "0") {
                    clearInterval(intId);
                    toolTipModel.data.tip.remove();
                }
            }, toolTipModel.data.fadeSpeed);
            toolTipModel.data.tip.remove();
            $("#phoneNumberToolTip").hide();
        }
    };
    var toolTipController = {
        handler: function () {
            toolTipModel.data.a.addEventListener("mouseover", toolTipView.showTip, false);
            toolTipModel.data.a.addEventListener("mouseout", toolTipView.hideTip, false);
        }
    };
    toolTipController.handler();
});